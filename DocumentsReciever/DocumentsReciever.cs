﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;


namespace Docu_Reciever
{
    public class DocumentsReciever
    {
        public delegate void DocuReady();
        public event DocuReady DocumentsReady;

        public delegate void TimeIsOver(object sender, ElapsedEventArgs args);
        public event TimeIsOver TimeOut;

        private string _defaultPath;
        private FileSystemWatcher _fileWatcher;

        private List<string> _docuList;
        private int _filesLeftToDownload = default;
        private Timer _timer;


        static void Main ()
        {

        }
        
        public DocumentsReciever(List<string> documentsToDownloadList)
        {
            _docuList = documentsToDownloadList;
        }
        public void Start(string path, int timeExpiredTime = 6000)
        {
            _filesLeftToDownload = _docuList.Count; // определяем количество файлов к загрузке
            if (String.IsNullOrEmpty(path)) // проверяем что переданный путь не пустой
            {
                _defaultPath = Path.Combine(Environment.CurrentDirectory, "DocuDownload");
                if (!Directory.Exists(_defaultPath))
                {
                    try
                    {
                        Directory.CreateDirectory(_defaultPath);
                        _fileWatcher = new FileSystemWatcher(_defaultPath);
                    }
                    catch { Console.WriteLine($"Failed to create folder {_defaultPath}"); }
                }
            }
            else
            {
                if (!Directory.Exists(path))
                {
                    path = Path.Combine(Environment.CurrentDirectory, path);
                    try
                    {
                        Directory.CreateDirectory(path);
                        _fileWatcher = new FileSystemWatcher(path);
                    }
                    catch { Console.WriteLine($"Failed to create folder {path}"); }
                }
                _fileWatcher = new FileSystemWatcher(path);
            }
            _timer = new Timer(timeExpiredTime);
            _timer.Enabled = true;
            // Добавляем обработчики для событий ->
            _fileWatcher.EnableRaisingEvents = true;
            _fileWatcher.Created += onCreated;
            _timer.Elapsed += timerIsElapsed;
            // Добавляем обработчики для событий <-
        }

        private void timerIsElapsed(object sender, ElapsedEventArgs e)
        {
            TimeOut?.Invoke(this, e);
            _fileWatcher.Dispose();
            _timer.Dispose();
        }
        public void onDocumentsReady()
        {
            _fileWatcher.Dispose();
            _timer.Dispose();
            DocumentsReady?.Invoke(); // вызываем событие DocumentsReady и передаём обработчику события
        }

        private void onCreated(object sender, FileSystemEventArgs e)
        {
            if (checkUploadedFile(e.Name))
            {
                Console.WriteLine($"File {e.Name} was uploaded! Remained {_filesLeftToDownload} files");
            }
            else
            {
                Console.WriteLine($"File {e.Name} doesn't exist in mandatory files list!\nFile will be deleted!");
                File.Delete(e.FullPath); // если файла нет в списке то удаляем его.
            }
        }

        private bool checkUploadedFile(string fileName)
        {

            if (_docuList.Contains(fileName)) // проверяем что загруженный файл есть в списке файлов
            {
                if (_filesLeftToDownload != 0) // проверяем что счётчик загруженных файлов ещё не обнулён
                {
                    _filesLeftToDownload -= 1; // декрементим счёчик оставшихся файлов к загрузке
                    if (_filesLeftToDownload == 0)  // если счётчик уже обнулился то больше файлы принимать не будем
                    {
                        onDocumentsReady();
                    }
                    return true;
                }
            }
            return false;

        }
    }
}

