﻿using Docu_Reciever;
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;

namespace Otus_delegates
{
    class Program_new
    {
        static bool _exit = false;
        public static void Main()
        {
            DocumentsReciever documentReciever = new DocumentsReciever(new List<string>() { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" });

            documentReciever.TimeOut += timeIsExpired;
            documentReciever.DocumentsReady += docuReady;

            documentReciever.Start("DocuDownload", 15000);
            while (!_exit)
            { 
            }
        }
        static void  docuReady()
        {
            Console.WriteLine("All required files is already downloaded");
            _exit = true;
        }
        static void timeIsExpired(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine($"The time is over at...\n{e.SignalTime}");
            _exit = true;
        }
    }
}
